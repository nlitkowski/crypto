# Kryptografia z Elementami Algebry
## Wydział Matematyki i Informatyki UAM
### IV semestr Informatyki, 2020-2021
---
#### Laboratory 1
+ [Slides](materials/lab1_slides.pdf)
+ [Tasks](materials/lab1_tasks.pdf)
+ [Solutions](src/lab1.py)
---
I claim no rights to files under the [_materials_](materials) subdirectory. All the rights belong to Prof. Maciej Grześkowiak.