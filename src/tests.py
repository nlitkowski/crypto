from utils import binary_power

if __name__ == "__main__":
    test_cases = [
        [5, 3],
        [6, 8],
        [10, 10],
        [
            12421512512531252616124361465143261,
            12346146412361432764137161661234643161341346,
        ],
    ]
    for case in test_cases:
        print(f"{case[0]}^{case[1]}={binary_power(case[0], case[1])}")
