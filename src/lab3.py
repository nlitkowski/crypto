from utils import *
from random import getrandbits, randrange, randint
from math import pow

INF = float("inf")


class EllipticCurve:
    def __init__(self, p_length=128, A=None, B=None, p=None):
        self.p = p or generate_random_prime(p_length)
        self.A = A
        self.B = B
        self.points = []

        if (A or B) is None:
            while True:
                self.A = randint(2, self.p)
                self.B = randint(2, self.p)
                d = (4 * binary_power(self.A, 3, self.p)) % self.p + (
                    27 * binary_power(self.B, 2, self.p)
                ) % self.p
                if d != 0:
                    break

    def print_curve_equation(self):
        print("Y^2 = X^3 + {} * X + {}\n".format(str(self.A), str(self.B), str(self.p)))

    def sum_points(self, P1: [int, int], P2: [int, int]):
        x1 = P1[0]
        y1 = P1[1]
        x2 = P2[0]
        y2 = P2[1]
        # P + O = P
        if x2 == INF and y2 == INF:
            print("P + O = P = ({}, {})\n".format(str(x1), str(y1)))
            return [x1, y1]
        elif x1 == INF and y1 == INF:
            print("O + P = P = ({}, {})\n".format(str(x2), str(y2)))
            return [x2, y2]
        elif x1 != x2:
            u = gcd(x1 - x2, self.p)
            lamb = ((y1 - y2) % self.p) * u
            x3 = (pow(lamb, 2) - x1 - x2) % self.p
            y3 = ((lamb * (x1 - x3) % self.p) - y1) % self.p
            print("P + Q = ({}, {})\n".format(str(x3), str(y3)))
            return [x3, y3]
        elif x1 == x2 and y1 == y2:
            u = gcd(2 * y1, self.p)
            lamb = (3 * pow(x1, 2) + self.A) * u
            x3 = (pow(lamb, 2) - 2 * x1) % self.p
            y3 = ((lamb * (x1 - x3) % self.p) - y1) % self.p
            print("P + P = ({}, {})\n".format(str(x3), str(y3)))
            return [x3, y3]
        elif x1 == x2 and y1 == self.p - y2:
            print("P + Q = P + (-P) = O \n")
            return [INF, INF]
        else:
            print("Error")
            return [None, None]

    def generate_points(self, n=1):
        for _ in range(n):
            while True:
                x = randint(2, self.p)
                fx = pow(x, 3) + self.A * x + self.B
                if euler_test(fx, self.p):
                    y = binary_power(fx, (self.p + 1) // 4, self.p)
                    if n == 1:
                        return [x, y]
                    self.points.append([x, y])
                    break

    def print_points(self):
        for point in self.points:
            print("({}, {})\n".format(str(point[0]), str(point[1])))

    def check_is_point_in_curve(self, P: [int, int]):
        x = P[0]
        y = P[1]
        if (pow(y, 2)) % self.p == (pow(x, 3) + self.A * x + self.B) % self.p:
            print("Point ({}, {}) belongs to curve\n".format(str(x), str(y)))
            return True
        else:
            print("Point ({}, {}) doesn't belong to curve\n".format(str(x), str(y)))
            return False

    def get_oposite_point(self, P: [int, int]):
        return [P[0], self.p - P[1]]


p = 6224595600485602748222159624129832522159491595880854185387618047680861494641730313786161491
A = 5077194311466834248357857920990646418574208657914218013694479585539298741963953373649737026
B = 686545343119020774474035339528534635174094942067514521103586530915476730988170815265826797

Px = 5581582864730824689967164886714414774849980855481576935395650940007676405818386972109810761

Py = 758960021073341251601042245899514655270348910962267934140292315358934522860095679819686590

Qx = 270176256755404810086017671769871841700712727671517086414289407707305572656973361663891241

Qy = 1589788089428067064085213215147034393925196906308979964665342216035499665239790256188539958


P = [Px, Py]
Q = [Qx, Qy]

c = EllipticCurve(A=A, B=B, p=p)
c.sum_points(P, Q)
c.sum_points(Q, P)
c.sum_points(P, P)
