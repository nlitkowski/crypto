from utils import generate_prime_number, binpow, euler_test, rnwd, sqrt_zn
from random import randrange, getrandbits, randint


class EllipticCurve:
    def __init__(self, l=128, A=None, B=None, p=None):
        self.p = p
        self.A = A
        self.B = B
        self.points = []
        self.k = 20

        if p is None:
            self.p = generate_prime_number(length=l)

        if A is None or B is None:
            while True:
                self.A = randint(2, self.p)
                self.B = randint(2, self.p)
                d = (4 * binpow(self.A, 3, self.p)) % self.p + (
                    27 * binpow(self.B, 2, self.p)
                ) % self.p
                if d != 0:
                    break

    def print_curve(self):
        print(
            "E: Y^2 = X^3 + {}X + {} over F{} \n".format(
                str(self.A), str(self.B), str(self.p)
            )
        )

    def print_coefficients(self):
        print("p -> {}".format(str(self.p)))
        print("A -> {}".format(str(self.A)))
        print("B -> {}".format(str(self.B)))

    def print_points(self):
        for point in self.points:
            print("({}, {})\n".format(str(point[0]), str(point[1])))

    def change_params(self, A, B, p):
        self.A = A
        self.B = B
        self.p = p

    def generate_points(self, n=1):
        """ n - number of points to generate """
        for _ in range(n):
            while True:
                x = randint(2, self.p)
                xd = x ** 3 + self.A * x + self.B
                if euler_test(xd, self.p):
                    y = binpow(xd, (self.p + 1) // 4, self.p)
                    if n == 1:
                        return [x, y]
                    self.points.append([x, y])

                    break

    def check_point(self, P):
        x = P[0]
        y = P[1]
        if (y ** 2) % self.p == (x ** 3 + self.A * x + self.B) % self.p:
            # print("Point ({}, {}) belongs to curve\n".format(str(x), str(y)))
            return True
        else:
            # print("Point ({}, {}) doesn't belong to curve\n".format(str(x), str(y)))
            return False

    def opposite_point(self, P: [int, int]):
        x = P[0]
        y = P[1]
        # print("Opposite point -> ({}, {})\n".format(str(x), str(self.p - y)))
        return [x, self.p - y]

    def n_p(self, n, P):
        Q = P
        R = [float("inf"), float("inf")]
        while n > 0:
            if n % 2 == 1:
                R = self.sum_points(R, Q)
                n -= 1
            Q = self.sum_points(Q, Q)
            n = n // 2
        return R

    def find_point(self, M):
        flag = False
        x = self.k * M
        while flag != True:
            y = sqrt_zn(x ** 3 + self.A * x + self.B, self.p)
            if y != False:
                if self.check_point([x, y]):
                    flag = True
                else:
                    x += 1
            else:
                x += 1
        return [x, y]

    def sum_points(self, P1: [int, int], P2: [int, int]):
        x1 = P1[0]
        y1 = P1[1]
        x2 = P2[0]
        y2 = P2[1]
        # P + O = P
        if x2 == float("inf") and y2 == float("inf"):
            return [x1, y1]
        # O + P = P
        elif x1 == float("inf") and y1 == float("inf"):
            return [x2, y2]
        # P + Q = R
        elif x1 != x2:
            u = rnwd((x2 - x1) % self.p, self.p)
            lamb = ((y2 - y1) % self.p) * u
            x3 = (lamb ** 2 - x1 - x2) % self.p
            y3 = (lamb * (x1 - x3) - y1) % self.p
            return [x3, y3]
        # P + P = 2P = Q
        elif x1 == x2 and y1 == y2:
            u = rnwd(2 * y1, self.p)
            lamb = (3 * x1 ** 2 + self.A) * u
            x3 = (lamb ** 2 - 2 * x1) % self.p
            y3 = ((lamb * (x1 - x3) % self.p) - y1) % self.p
            return [x3, y3]
        # P + Q = O -> Q = -P
        elif x1 == x2 and y1 == self.p - y2:
            return [float("inf"), float("inf")]
        else:
            print("Error")
            return [None, None]
