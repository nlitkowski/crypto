from random import randint, sample
from math import log, floor
from utils import *

# 1. Zaimplementuj algorytm (funkcję), która generuje losowy element zbioru Zn (0, 1, ..., n-1).
def first(k, n):
    """Solution to first task"""
    print(f"k: {k}")
    print(f"n: {n}")
    try:
        result = zn_random(k, n)
        print(f"result: {result}")
    except Exception as e:
        print(f"{e}")


# 2. Zaimplementuj algorytm (funkcję) obliczania odwrotności w grupie Φ(n).
# Wykorzystaj Rozszerzony Algorytm Euklidesa
def second(n: int, b: int):
    """Solution to second task"""
    d = gcd(b, n)
    if d == 1:
        z = int(d / b) % n
        print(f"b^-1 = {z}")


# 3. Zaimplementuj algorytm (funkcje) efektywnego potegowania w zbiorze Z^*n.
# Wykorzystaj algorytm iterowanego podnoszenia do kwadratu.
def third(n: int, b: int, k: int):
    """Solution to third task"""
    print(f"Result: {exponentiation_mod_n(n, b, k)}")


# 4. Niech p bedzie liczba pierwsza. Zaimplementuj test (funkcje), który sprawdza czy
# element zbioru Z^* p jest reszta kwadratowa w Z^*p. Wykorzystaj twierdzenie Eulera.
def fourth(n, b):
    """Solution to fourth task"""
    try:
        print(f"Result: {is_quadratic_res(n, b)}")
    except Exception as e:
        print(f"{e}")


# 5. Zaimplementuj algorytm (funkcje), który oblicza pierwiastek kwadratowy
# w ciele F^*p, gdzie p == 3 (mod 4) jest liczba pierwsza. Wykorzystaj twierdzenie Eulera.
def fifth(p, b):
    """Solution to fifth task"""
    try:
        if is_quadratic_res(p, b):
            if p % 4 == 3:
                a = exponentiation_mod_n(p, b, ((p + 1) // 4))
                print(f"Result: {a}")
            else:
                print("Could not find square root")
        else:
            print("Could not find square root")
    except Exception as e:
        print(f"{e}")


# 6. Zaimplementuj test (funkcję), który sprawdza liczba naturalna n jest liczba pierwsza.
# Wykorzystaj test Fermata
def sixth(n):
    """Solution to sixth task"""
    print(f"Result: {is_primal(n)}")


def main():
    b = 646364623469634716329421551581459444393459634563465364563456387456873465873645876345876345876345876345876345863458763458763485763485763487563845638465837465834658765735646345645856346875
    k = 7268263486823646238468236462384682364586385634856834658634586348658365873645874658734658736458763875638475683765834658346586856348756873465863456
    n = 943923749729479745795479759798579759734597345979578937597359739587398573985793875983759834759735973459873459734985739857359793573598734975983745983745973495734957394579347593847593759734597345973497349857394759347593745934793475973459734957394759374597349573457934759347597345973459734957394579379579579374597359735975173

    print("\nFirst task\n")
    first(k, n)

    print("\nSecond task\n")
    second(n, b)

    print("\nThird task\n")
    third(n, b, k)

    print("\nFourth task\n")
    fourth(n, b)

    print("\nFifth task\n")
    fifth(n, k)

    print("\nSixth task\n")
    sixth(n)


if __name__ == "__main__":
    main()
